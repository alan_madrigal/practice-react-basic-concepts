import { createElement, React } from "react";
import logo from "./logo.svg";
import "./App.css";

const PLANETS = [
  "Mercury",
  "Venus",
  "Earth",
  "Mars",
  "Jupiter",
  "Saturn",
  "Uranus",
  "Neptune",
];

function Planets() {
  return <div >
    <ul className="planets-list" >
    {PLANETS.map((planet ,index)=> 
    <li  key={index}>
    {planet}
    </li>

    )}
    </ul>
  </div>;
}

function App() {
  return (
    <div className="App">
    {createElement("h1", {style :{color :'#999', fontSize: '19px'}}, "Solar system  planets:")}
    <Planets />
    </div>

  );
}

export default App;
